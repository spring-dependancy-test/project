package com.hnr.base;

import com.hnr.commons.Book;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class BaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseApplication.class, args);

		Book book = new Book();
		book.setId(1);
		book.setName("Test");
		book.setDate(new Date());
        System.out.println(book);
	}
}
